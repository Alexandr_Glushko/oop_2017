﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programm
{
    public class Student
    {
        private string _firstName;
        private string _lastName;
        private string _groupName;
        private int _age;
        private string _sex;

        public string FirstName { get { return _firstName; } set { _firstName = value; } }
        public string LastName { get { return _lastName; } set { _lastName = value; } }
        public string GroupName { get { return _groupName; } set { _groupName = value; } }
        public string Sex { get { return _sex; } set { _sex = value; } }
        public int Age { get { return _age; } set {_age = value; } }
        public void Input()
        {
            Console.WriteLine("Input FirstName, LastName, GroupName, Sex and Age through the Enter");
            FirstName = Console.ReadLine();
            LastName = Console.ReadLine();
            GroupName = Console.ReadLine();
            while (true)
            {
                try
                {
                    string input = Console.ReadLine();
                    if (String.Compare(input, "Male")==0|| String.Compare(input, "Female")==0)
                    {
                        Sex = input;
                        break;
                    }
                    else
                    {
                        throw new Exception("Wrong input, people have only 2 possible sex");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: {0}", e.Message);
                }
                
            }
            while (true)
            {
                try
                {
                    string input = Console.ReadLine();
                    if (Int32.TryParse(input,out int Agee)) // "TryParse" true if convert is successful
                    {
                        Age = Convert.ToInt32(input);
                        if (Age < 0)
                        {
                            Console.WriteLine("Age can not be less than 0");
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        throw new Exception("Wrong input, age can only be a figure");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: {0}", e.Message);
                }
            }
        }
        public override string ToString()
        {
            return string.Format("{0} {1} \n{2} {3} \n{4}", _firstName, _lastName, _sex, _age, _groupName);
        }
    } 
}      
       