﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programm
{
    class Program
    {
        static void Main(string[] args)
        {
            var JohnDoe = new Student
            {
                FirstName = "John",
                LastName = "Doe",
                GroupName = "KNIT-16-1",
                Age = 18,
                Sex = "Male"
            };
            JohnDoe.Input();
            System.Console.Clear();
            Console.WriteLine(JohnDoe);
            Console.Read();
        }
    }
}
