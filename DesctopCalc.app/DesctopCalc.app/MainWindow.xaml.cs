﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DesctopCalc.app
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string func;
        double a;
        double b;
        bool flagC = true;
        bool flagOut = true;
        bool flagRes = false;
        private void BtnNum_Click(object sender, RoutedEventArgs e)
        {
            if (flagRes) BtnFuncClear_Click(sender,e);
            flagOut = true;
            var btNumber = sender as Button;
            if (btNumber != null)
            {
                var inputDate = Screen.Text;
                if (inputDate.Equals("0"))
                {
                    inputDate = $"{btNumber.Tag}";
                }
                else
                {
                    inputDate = $"{inputDate}{btNumber.Tag}";
                }
                Screen.Text = inputDate;
                flagRes = false;
            }
        }

        private void BtnFuncBs_Click(object sender, RoutedEventArgs e)
        {
            if (Screen.Text.Length == 1)
            {
                Screen.Text = "0";
                flagC = true;
                flagOut = false;
            }
            if (Screen.Text != "0") Screen.Text = Screen.Text.Remove(Screen.Text.Length - 1);
        }

        private void BtnFuncClear_Click(object sender, RoutedEventArgs e)
        {
            Screen.Text.Remove(0);
            Screen.Text = "0";
            flagC = true;
            flagOut = false;
        }

        public void BtnFuncPlus_Click(object sender, RoutedEventArgs e)
        {
            a = Convert.ToDouble(Screen.Text);
            Screen.Text.Remove(0);
            Screen.Text = "0";
            flagC = true;
            func = "add";
        }
        private void BtnFuncMinus_Click(object sender, RoutedEventArgs e)
        {
            a = Convert.ToDouble(Screen.Text);
            Screen.Text.Remove(0);
            Screen.Text = "0";
            flagC = true;
            func = "sub";
        }
        private void BtnFuncMult_Click(object sender, RoutedEventArgs e)
        {
            a = Convert.ToDouble(Screen.Text);
            Screen.Text.Remove(0);
            Screen.Text = "0";
            flagC = true;
            func = "mul";
        }

        private void BtnFuncDiv_Click(object sender, RoutedEventArgs e)
        {
            a = Convert.ToDouble(Screen.Text);
            Screen.Text.Remove(0);
            Screen.Text = "0";
            flagC = true;
            func = "div";
        }
        private void BtnFuncAnswer_Click(object sender, RoutedEventArgs e)
        {
            if (flagC)
            {
                b = Convert.ToDouble(Screen.Text);
                switch (func)
                {
                    case "add":
                        Screen.Text = Convert.ToString(a + b);
                        break;
                    case "sub":
                        Screen.Text = Convert.ToString(a - b);
                        break;
                    case "div":
                        Screen.Text = Convert.ToString(a / b);
                        break;
                    case "mul":
                        Screen.Text = Convert.ToString(a * b);
                        break;
                }
                flagC = false;
                flagOut = false;
                flagRes = true;
            }
            
        }

        private void BtnPoint_Click(object sender, RoutedEventArgs e)
        {
            bool flagPoint = Screen.Text.Contains(",");
            if (!flagPoint)
            {
                var lng = Screen.Text.Length;
                Screen.Text = Screen.Text.Insert(lng, ",");
            }     
        }
    }
}
   