﻿using System;
class Program
{
    static void Main(string[] args)
    {
        double side = 0;
        double length = 0;
        double width = 0;
        double height = 0;
        double baseoftriangle = 0;
        double radius = 0;
        UserChoice:
        Console.WriteLine("For what shape you want to calculate the Area:\n1. Sqaure\n2. Rectangle\n3. Triangle\n4. Circle");
        Console.Write("Please Select the number from above options: ");
        int choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Console.Write("Please enter the side of square: ");
                side = double.Parse(Console.ReadLine());
                break;
            case 2:
                Console.Write("Please enter the length of rectangle: ");
                length = double.Parse(Console.ReadLine());
                Console.Write("Please enter the width of rectangle: ");
                width = double.Parse(Console.ReadLine());
                break;
            case 3:
                Console.Write("Please enter the height of triangle: ");
                height = double.Parse(Console.ReadLine());
                Console.Write("Please enter the base of triangle: ");
                baseoftriangle = double.Parse(Console.ReadLine());
                break;
            case 4:
                Console.Write("Please enter the radius of circle: ");
                radius = double.Parse(Console.ReadLine());
                break;
            default:
                Console.WriteLine("Incorrect Choice, please try again!");
                goto UserChoice;
        }
        CalculateArea Sqa = new Square();
        CalculateArea Rec = new Rectangle();
        CalculateArea Tri = new Triangle();
        CalculateArea Cir = new Circle();
        if (choice == 1)
        {
            Sqa.Area(side);
            Sqa.ShowResult();
        }
        else if (choice == 2)
        {
            Rec.Area(length, width);
            Rec.ShowResult();
        }
        else if (choice == 3)
        {
            Tri.Area(height, baseoftriangle);
            Tri.ShowResult();
        }
        else
        {
            Cir.Area(radius);
            Cir.ShowResult();
        }
        ChoiceOfAnotherCalculation:
        Console.Write("\nDo you want to calculate area of any other shape? Give input in Yes or NO: ");
        string choice1 = Console.ReadLine();
        switch (choice1.ToUpper())
        {
            case "YES":
                goto UserChoice;
            case "NO":
                break;
            default:
                Console.WriteLine("Incorrect Choice, please try again!");
                goto ChoiceOfAnotherCalculation;
        }
    }
}
class CalculateArea
{
    public double result;
    public virtual void Area(double side)
    {
    }
    public virtual void Area(double length, double width)
    {
    }
    public void ShowResult()
    {
        Console.WriteLine($"Your Result is {result}");
    }
}
class Square : CalculateArea
{

    public override void Area(double side)
    {
        result = side * side;
    }

}
class Rectangle : CalculateArea
{
    public override void Area(double length, double width)
    {
        result = length * width;
    }
}
class Triangle : CalculateArea
{
    public override void Area(double height, double baseoftriangle)
    {
        result = (height * baseoftriangle) / 2;
    }
}
class Circle : CalculateArea
{
    public override void Area(double radius)
    {
        result = 3.14159 * radius * radius;
    }
}